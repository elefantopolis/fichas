$(document).ready(function(){

    var miTabla = $('#miTabla').DataTable({
        oLanguage: {
            sUrl: base_url+"site_media/js/spanish.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/trabajadores',
            type: 'POST'
        },
        columns: [
            { "data": "num" },
            { "data": "nombre" },
            { "data": "apellidos" },
            { "data": "edad" },
            { "data": "antiguedad" },
            { "data": "botones" }
        ],
        allFilters: true,
        scrollCollapse: true,
        deferRender: true,
        scroller: {
            loadingIndicador: false,
        }
    });

    $.validity.setup({ outputMode:"summary" });

    /*$("#imprimir").validity(function() {
        $(".required").require();
    });

    /*function enviar_formulario(){ 
       document.imprimir.submit();    
    }/**/

    $('#imprimir').submit(function(event) {
        event.preventDefault();
        if ( validateAll() ) {
            $('#modal-imprimir').modal('hide');
            document.imprimir.submit();
        }
    });

    function validateAll(){
    $.validity.start();
    $('.required').require();
    var result = $.validity.end();
    return result.valid;
}

    $('.date').datepicker({
        autoclose: true,
        format : 'dd/mm/yyyy'
    });

    $(".date").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});

    $(document).on('click', '.btn-eliminar', function(event) {
        event.preventDefault();
        $('#modal-eliminar').modal('show');
        $('#id').val($(this).data('id'));
    });

    $(document).on('click', '.btn-imprimir', function(event) {
        event.preventDefault();
        $('#modal-imprimir').modal('show');
        $('#empleado').val($(this).data('id'));
    });

    /*$(document).on('click', '.btn-generar', function(event) {
        event.preventDefault();
        
    });/**/

    

});

