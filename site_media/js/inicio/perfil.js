$(document).ready(function(){

    $('.file').fileinput({
        showUpload: false
    });

    $(document).on('click', '.btn-subir', function(event) {
        event.preventDefault();
        $('#modal-subir').modal('show');
        $('.nombre').html($(this).data('nombre'));
        $('.campo').val($(this).data('archivo'));
    });

    $(document).on('click', '.btn-cambiar', function(event) {
        event.preventDefault();
        $('#modal-cambiar').modal('show');
        $('.nombre').html($(this).data('nombre'));
        $('.campo').val($(this).data('archivo'));
    });

});

