$(document).ready(function(){

    var miTabla = $('#miTabla').DataTable({
        oLanguage: {
            sUrl: base_url+"site_media/js/spanish.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/trabajadores_eliminados',
            type: 'POST'
        },
        columns: [
            { "data": "num" },
            { "data": "nombre" },
            { "data": "apellidos" },
            { "data": "edad" },
            { "data": "antiguedad" },
            { "data": "botones" }
        ],
        allFilters: true,
        scrollCollapse: true,
        deferRender: true,
        scroller: {
            loadingIndicador: false,
        }
    });

    $(document).on('click', '.btn-activar', function(event) {
        event.preventDefault();
        $('#modal-activar').modal('show');
        $('#id').val($(this).data('id'));
    });

    

});

