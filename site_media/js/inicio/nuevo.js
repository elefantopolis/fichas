$(document).ready(function(){

    $.validity.setup({ outputMode:"summary" });

    $('.uppercase').change(function(event) {
        var element = $(this);
        element.val(element.val().toUpperCase());
    });

    $("#nuevo").validity(function() {
        $(".required").require();
        $(".number").match('number');
    });

    $('.date').datepicker({
        autoclose: true,
        format : 'dd/mm/yyyy'
    });

    $(".date").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
    $(".phone").mask("(999) 999-9999");

});

