-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 15-03-2017 a las 08:26:54
-- Versión del servidor: 5.7.16
-- Versión de PHP: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fichas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(4) NOT NULL,
  `trabajador` int(4) NOT NULL,
  `acta_nacimiento` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `curp` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `padres_1` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `padres_2` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `hijos_1` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `hijos_2` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `hijos_3` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `hijos_4` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `hijos_5` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `comprobante_domicilio` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `constancia_origen` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `cartilla_militar` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `carta_invitacion` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `primaria` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `secundaria` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `prepa` varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  `carrera` varchar(37) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `trabajador`, `acta_nacimiento`, `curp`, `padres_1`, `padres_2`, `hijos_1`, `hijos_2`, `hijos_3`, `hijos_4`, `hijos_5`, `comprobante_domicilio`, `constancia_origen`, `cartilla_militar`, `carta_invitacion`, `primaria`, `secundaria`, `prepa`, `carrera`) VALUES
(1, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `id` int(4) NOT NULL,
  `num_unico` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `dependencia` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nacimiento` date NOT NULL,
  `curp` varchar(18) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `num_seguro` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `domicilio` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `inicio_trabajo` date NOT NULL,
  `puesto` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `num_puesto` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pago` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`id`, `num_unico`, `dependencia`, `nombre`, `apellidos`, `nacimiento`, `curp`, `rfc`, `num_seguro`, `domicilio`, `telefono`, `inicio_trabajo`, `puesto`, `num_puesto`, `pago`, `activo`) VALUES
(1, '23800', '5390011460000000100', 'ADRIAN DOMITILO', 'HERRERA PACHECO', '1985-01-08', 'HEPA850108HOCRCD00', 'HEPA850108', '78058509825', 'CONOCIDO', '(999) 999-9999', '2010-06-16', '2P1329A', '9940', '6208.00', 1),
(2, '23801', '5390011460000000100', 'ISRAEL', 'MARTINEZ CARRASCO', '1987-07-08', 'MACI870708HOCRRS08', 'MACI870708DY5', '78059209825', 'CONOCIDO', '(983) 443-2213', '2014-03-16', '2P1329A', '123221', '5250', 1),
(3, '23802', '5390011460000000100', 'URIEL SILVESTRE', 'LOPEZ SACHIÑAS', '1986-08-01', 'LOSU860801HOCRRS04', 'LOSU860801', '75258509825', 'CONOCIDO', '(485) 749-3948', '2000-06-01', '2P1329A', '3423', '4600', 1),
(4, '23804', '5390011460000000100', 'GABRIELA DEL CARMEN', 'MARTINEZ GUZMAN', '1987-06-06', 'MAGG870606MOCRRS01', 'MAGG870606', '78058509855', 'CONOCIDO', '(948) 285-7392', '2017-03-16', '2P1329A', '1111', '4000', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trabajador` (`trabajador`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_ibfk_1` FOREIGN KEY (`trabajador`) REFERENCES `trabajador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
