<!DOCTYPE>
<html>
<head>
	<title></title>
</head>
<body>
	<?php foreach ($fechas as $dt):?>
		<?php
		$fv = explode('/', $dt['fin']);
		if ($fv[0] == 31) {
			$extra = $extra = $tr->pago/15;
		}else{
			$extra = 0;
		}

		if ($fv[1] == '05' or $fv[1] == 12) {
			$pago = $tr->pago * 2;
			$aguinaldo = $tr->pago / 2;
		}
		else{
			$pago = $tr->pago;
			$aguinaldo = 0;
		}

		$validacion = strtotime($fv[2].'/'.$fv[1].'/'.$fv[0]);
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
		$years = floor($diff / (365*60*60*24));
		$anios = round($years/5);	

		//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		die();

		$psm = (18.18/100) * $tr->pago;
		$despensa = (7.41/100) * $tr->pago;
		$act_cul_dep = (6.193/100) * $tr->pago;
		$quiniquientos = (10.173/100) * $tr->pago;

		if ($anios != 0) {
			$valor = $anios * $quiniquientos;
		}else{
			$valor = 0;
		}
		
		$total1 = $pago + $aguinaldo + $extra + $psm + $despensa + $act_cul_dep + $valor;

		//Deducciones

		$seguro = (13.14/100) * $tr->pago;
		$pension = (9/100) * $tr->pago;
		$cuota_sin = (1/100) * $tr->pago;
		$cuota_imss = (4.12/100) * $tr->pago;
		$bca = (2.24/100) * $tr->pago;
		$mat = (2.01/100)  * $tr->pago;

		$total2 = $seguro + $pension + $cuota_sin + $cuota_imss + $bca + $mat;
		?>
	<br>
	<table cellpadding="3" cellspacing="0" border="0" width="100%" style="font-size: 10px; text-align: center;">
		<tr>
			<td width="5%"><label style="font-size: 10px;"></label></td>
			<td width="10%"><label style="font-size: 10px;"><?php echo $tr->num_unico?></label></td>
			<td width="15%"><label style="font-size: 10px;"><?php echo $dt['fin']?></label></td>
			<td width="20%"><label style="font-size: 10px;"><?php echo $tr->dependencia?></label></td>
			<td width="50%"><label style="font-size: 10px;"><?php echo $tr->apellidos.' '.$tr->nombre?></label></td>
		</tr>
		<tr>
			<td width="9%"><label style="font-size: 10px;"><br><br><?php echo $tr->puesto?></label></td>
			<td width="5%"><label style="font-size: 10px;"><br><br><?php echo $tr->num_puesto?></label></td>
			<td width="15%"><label style="font-size: 10px;"><br><br><?php echo $dt['inicio']?></label></td>
			<td width="15%"><label style="font-size: 10px;"><br><br><?php echo $dt['fin']?></label></td>
			<td width="3%"><label style="font-size: 10px;"><br><br>0</label></td>
			<td width="3%"><label style="font-size: 10px;"><br><br>0</label></td>
			<td width="3%"><label style="font-size: 10px;"><br><br>0</label></td>
			<td width="15%"><label style="font-size: 10px;"><br><br><?php echo $tr->num_seguro?></label></td>
			<td width="32%"><label style="font-size: 10px;"><br><?php echo $tr->curp.'<br>'.$tr->rfc?></label></td>
		</tr>
		<tr>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 10px;">
					<tr>
						<td colspan="3"><br><br></td>
					</tr>
					<tr>
						<td width="10%"><label style="text-align: right; font-size: 10px;">1</label></td>
						<td width="65%"><label style="text-align: left; font-size: 10px;">SUELDO</label></td>
						<td width="25%"><label style="text-align: right; font-size: 10px;"><?php echo number_format($pago, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">3</label></td>
						<td><label style="text-align: left; font-size: 10px;">DIA AJUSTE</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo ($extra != 0) ? number_format($extra, 2) : ''?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">4</label></td>
						<td><label style="text-align: left; font-size: 10px;">P.M.S.</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($psm, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">5</label></td>
						<td><label style="text-align: left; font-size: 10px;">DESPENSA</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($despensa, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">6</label></td>
						<td><label style="text-align: left; font-size: 10px;">ACT. CUL Y DEP.</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($act_cul_dep, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">12</label></td>
						<td><label style="text-align: left; font-size: 10px;">QUINQUINIENTOS (<?php echo $anios?>)</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($valor, 2)?></label></td>
					</tr>
					<?php if ($fv[1] == '05' or $fv[1] == 12):?>
					<tr>
						<td><label style="text-align: right; font-size: 10px;"></label></td>
						<td><label style="text-align: left; font-size: 10px;">AGUINALDO</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($aguinaldo, 2)?></label></td>
					</tr>
					<?php endif;?>
					<tr>
						<td><label style="text-align: right; font-size: 10px;"></label></td>
						<td><label style="text-align: right; font-size: 10px;">TOTAL PERCEPSIONES $</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($total1, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 10px;">
					<tr>
						<td colspan="3"><br><br></td>
					</tr>
					<tr>
						<td width="15%"><label style="text-align: right; font-size: 10px;">201</label></td>
						<td width="65%"><label style="text-align: left; font-size: 10px;">I.S.S.S.</label></td>
						<td width="20%"><label style="text-align: right; font-size: 10px;"><?php echo number_format($seguro, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">202</label></td>
						<td><label style="text-align: left; font-size: 10px;">FONDO PENSIONES</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($pension, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">203</label></td>
						<td><label style="text-align: left; font-size: 10px;">CUOTA SINDICAL</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($cuota_sin, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">204</label></td>
						<td><label style="text-align: left; font-size: 10px;">CUOTA IMSS</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($cuota_imss, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">226</label></td>
						<td><label style="text-align: left; font-size: 10px;">L. BCA (DRH) 28/36</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($bca, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 10px;">227</label></td>
						<td><label style="text-align: left; font-size: 10px;">P. MAT(DRH) 54/72</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($mat, 2)?></label></td>
					</tr>
					<?php if ($fv[1] == '05' or $fv[1] == 12):?>
					<tr>
						<td><label style="text-align: right; font-size: 10px;"></label></td>
						<td><label style="text-align: left; font-size: 10px;"></label></td>
						<td><label style="text-align: right; font-size: 10px;"></label></td>
					</tr>
					<?php endif;?>
					<tr>
						<td><label style="text-align: right; font-size: 10px;"></label></td>
						<td><label style="text-align: right; font-size: 10px;">TOTAL DEDUCCIONES $</label></td>
						<td><label style="text-align: right; font-size: 10px;"><?php echo number_format($total2, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 10px;">
					<tr>
						<td><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><?php if ($fv[1] == '05' or $fv[1] == 12):?><p></p><?php endif;?></td>
					</tr>
					<tr>
						<td><label style="text-align: center; font-size: 10px;">$ <?php echo number_format($total1 - $total2, 2)?></label></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p></p><p></p>
	<?php endforeach;?>
</body>
</html>