<!DOCTYPE>
<html>
<head>
	<title></title>
</head>
<body>
	<?php if(!empty($fechas[($hoja*3)-3]['inicio'])):?>
	<?php
		$inicio = $fechas[($hoja*3)-3]['inicio']; 
		$fin = $fechas[($hoja*3)-3]['fin']; 

		$ftra = explode('-', $tr->inicio_trabajo);
		$fi = explode('/', $inicio);

		$fv = explode('/', $fin);
		if ($fv[0] == 31) {
			$extra = $extra = $tr->pago/15;
		}else{
			$extra = 0;
		}

		if ($fv[1] == '05' or $fv[1] == 12) {
			$pago = $tr->pago * 2;
			$aguinaldo = $tr->pago / 2;
		}
		else{
			$pago = $tr->pago;
			$aguinaldo = 0;
		}

		$validacion = strtotime($fv[2].'/'.$fv[1].'/'.$fv[0]);
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
		$years = floor($diff / (365*60*60*24));
		$anios = round($years/5);	

		//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		die();

		$psm = (18.18/100) * $tr->pago;
		$despensa = (7.41/100) * $tr->pago;
		$act_cul_dep = (6.193/100) * $tr->pago;
		$quiniquientos = (10.173/100) * $tr->pago;

		//echo "<pre>"; print_r($fi); print_r($ftra); print_r($fv); die();
		$valor = 0;
		if ($anios != 0) {
			if (((int)$ftra[2] <= (int)$fi[0] || (int)$ftra[2] >= (int)$fv[0]) && (int)$ftra[1] == (int)$fv[1]) {
				$valor = $anios * $quiniquientos;
			} 
		}
		
		$total1 = $pago + $aguinaldo + $extra + $psm + $despensa + $act_cul_dep + $valor;

		//Deducciones

		$seguro = (13.14/100) * $tr->pago;
		$pension = (9/100) * $tr->pago;
		$cuota_sin = (1/100) * $tr->pago;
		$cuota_imss = (4.12/100) * $tr->pago;
		$bca = (2.24/100) * $tr->pago;
		$mat = (2.01/100)  * $tr->pago;

		$total2 = $seguro + $pension + $cuota_sin + $cuota_imss + $bca + $mat;

		$total3 = $total1 - $total2;
	?>
	<table cellpadding="3" cellspacing="0" border="0" width="100%" style="font-size: 8.5px; text-align: center;">
		<tr>
			<td width="5%"><label style="font-size: 8.5px;"></label></td>
			<td width="10%"><label style="font-size: 8.5px;"><?php echo $tr->num_unico?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><?php echo $fin?></label></td>
			<td width="20%"><label style="font-size: 8.5px;"><?php echo $tr->dependencia?></label></td>
			<td width="50%"><label style="font-size: 8.5px;"><?php echo $tr->apellidos.' '.$tr->nombre?></label></td>
		</tr>
		<tr>
			<td width="9%"><label style="font-size: 8.5px;"><br><?php echo $tr->puesto?></label></td>
			<td width="5%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_puesto?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $inicio?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $fin?></label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_seguro?></label></td>
			<td width="32%"><label style="font-size: 8.5px;"><?php echo $tr->curp.'<br>'.$tr->rfc?></label></td>
		</tr>
		<tr>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="10%"><label style="text-align: right; font-size: 8.5px;">1</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">SUELDO</label></td>
						<td width="25%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pago, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">3</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DIA AJUSTE</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo ($extra != 0) ? number_format($extra, 2) : ''?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">4</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P.M.S.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($psm, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">5</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DESPENSA</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($despensa, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">6</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">ACT. CUL Y DEP.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($act_cul_dep, 2)?></label></td>
					</tr>
					<?php if($valor != 0):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">12</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">QUINQUINIENTOS (<?php echo $anios?>)</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($valor, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<?php if ($fv[1] == '05' or $fv[1] == 12):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;">AGUINALDO</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($aguinaldo, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total1, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="15%"><label style="text-align: right; font-size: 8.5px;">201</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">I.S.S.S.</label></td>
						<td width="20%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($seguro, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">202</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">FONDO PENSIONES</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pension, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">203</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA SINDICAL</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_sin, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">204</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA IMSS</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_imss, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">226</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">L. BCA (DRH) 28/36</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($bca, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">227</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P. MAT(DRH) 54/72</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($mat, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total2, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></td>
					</tr>
					<tr>
						<td><label style="text-align: center; font-size: 8.5px;">$ <?php echo number_format($total3, 2)?></label></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p></p><p></p><p></p><p></p>
	<?php endif;?>
	<?php if(!empty($fechas[($hoja*3)-2]['inicio'])):?>
	<?php
		$inicio = $fechas[($hoja*3)-2]['inicio']; 
		$fin = $fechas[($hoja*3)-2]['fin'];

		$ftra = explode('-', $tr->inicio_trabajo);
		$fi = explode('/', $inicio);

		$fv = explode('/', $fin);
		if ($fv[0] == 31) {
			$extra = $extra = $tr->pago/15;
		}else{
			$extra = 0;
		}

		if ($fv[1] == '05' or $fv[1] == 12) {
			$pago = $tr->pago * 2;
			$aguinaldo = $tr->pago / 2;
		}
		else{
			$pago = $tr->pago;
			$aguinaldo = 0;
		}

		$validacion = strtotime($fv[2].'/'.$fv[1].'/'.$fv[0]);
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
		$years = floor($diff / (365*60*60*24));
		$anios = round($years/5);	

		//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		die();

		$psm = (18.18/100) * $tr->pago;
		$despensa = (7.41/100) * $tr->pago;
		$act_cul_dep = (6.193/100) * $tr->pago;
		$quiniquientos = (10.173/100) * $tr->pago;

		//echo "<pre>"; print_r($fi); print_r($ftra); print_r($fv); die();
		$valor = 0;
		if ($anios != 0) {
			if (($ftra[2] >= $fi[0] || $ftra[2] <= $fv[0]) && $ftra[1] == $fv[1]) {
				$valor = $anios * $quiniquientos;
			}
		}
		
		$total1 = $pago + $aguinaldo + $extra + $psm + $despensa + $act_cul_dep + $valor;

		//Deducciones

		$seguro = (13.14/100) * $tr->pago;
		$pension = (9/100) * $tr->pago;
		$cuota_sin = (1/100) * $tr->pago;
		$cuota_imss = (4.12/100) * $tr->pago;
		$bca = (2.24/100) * $tr->pago;
		$mat = (2.01/100)  * $tr->pago;

		$total2 = $seguro + $pension + $cuota_sin + $cuota_imss + $bca + $mat;

		$total3 = $total1 - $total2;
	?>
	<table cellpadding="3" cellspacing="0" border="0" width="100%" style="font-size: 8.5px; text-align: center;">
		<tr>
			<td width="5%"><label style="font-size: 8.5px;"></label></td>
			<td width="10%"><label style="font-size: 8.5px;"><?php echo $tr->num_unico?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><?php echo $fin?></label></td>
			<td width="20%"><label style="font-size: 8.5px;"><?php echo $tr->dependencia?></label></td>
			<td width="50%"><label style="font-size: 8.5px;"><?php echo $tr->apellidos.' '.$tr->nombre?></label></td>
		</tr>
		<tr>
			<td width="9%"><label style="font-size: 8.5px;"><br><?php echo $tr->puesto?></label></td>
			<td width="5%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_puesto?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $inicio?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $fin?></label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_seguro?></label></td>
			<td width="32%"><label style="font-size: 8.5px;"><?php echo $tr->curp.'<br>'.$tr->rfc?></label></td>
		</tr>
		<tr>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="10%"><label style="text-align: right; font-size: 8.5px;">1</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">SUELDO</label></td>
						<td width="25%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pago, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">3</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DIA AJUSTE</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo ($extra != 0) ? number_format($extra, 2) : ''?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">4</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P.M.S.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($psm, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">5</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DESPENSA</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($despensa, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">6</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">ACT. CUL Y DEP.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($act_cul_dep, 2)?></label></td>
					</tr>
					<?php if($valor != 0):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">12</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">QUINQUINIENTOS (<?php echo $anios?>)</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($valor, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<?php if ($fv[1] == '05' or $fv[1] == 12):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;">AGUINALDO</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($aguinaldo, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total1, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="15%"><label style="text-align: right; font-size: 8.5px;">201</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">I.S.S.S.</label></td>
						<td width="20%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($seguro, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">202</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">FONDO PENSIONES</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pension, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">203</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA SINDICAL</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_sin, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">204</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA IMSS</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_imss, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">226</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">L. BCA (DRH) 28/36</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($bca, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">227</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P. MAT(DRH) 54/72</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($mat, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total2, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></td>
					</tr>
					<tr>
						<td><label style="text-align: center; font-size: 8.5px;">$ <?php echo number_format($total3, 2)?></label></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p></p><p></p><p></p><p></p><p></p>
	<?php endif;?>
	<?php if(!empty($fechas[($hoja*3)-1]['inicio'])):?>
	<?php
		$inicio = $fechas[($hoja*3)-1]['inicio']; 
		$fin = $fechas[($hoja*3)-1]['fin'];

		$ftra = explode('-', $tr->inicio_trabajo);
		$fi = explode('/', $inicio); 

		$fv = explode('/', $fin);
		if ($fv[0] == 31) {
			$extra = $extra = $tr->pago/15;
		}else{
			$extra = 0;
		}

		if ($fv[1] == '05' or $fv[1] == 12) {
			$pago = $tr->pago * 2;
			$aguinaldo = $tr->pago / 2;
		}
		else{
			$pago = $tr->pago;
			$aguinaldo = 0;
		}

		$validacion = strtotime($fv[2].'/'.$fv[1].'/'.$fv[0]);
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
		$years = floor($diff / (365*60*60*24));
		$anios = round($years/5);	

		//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		die();

		$psm = (18.18/100) * $tr->pago;
		$despensa = (7.41/100) * $tr->pago;
		$act_cul_dep = (6.193/100) * $tr->pago;
		$quiniquientos = (10.173/100) * $tr->pago;

		//echo "<pre>"; print_r($fi); print_r($ftra); print_r($fv); die();
		$valor = 0;
		if ($anios != 0) {
			if (($ftra[2] >= $fi[0] || $ftra[2] <= $fv[0]) && $ftra[1] == $fv[1]) {
				$valor = $anios * $quiniquientos;
			} 
		}
		
		$total1 = $pago + $aguinaldo + $extra + $psm + $despensa + $act_cul_dep + $valor;

		//Deducciones

		$seguro = (13.14/100) * $tr->pago;
		$pension = (9/100) * $tr->pago;
		$cuota_sin = (1/100) * $tr->pago;
		$cuota_imss = (4.12/100) * $tr->pago;
		$bca = (2.24/100) * $tr->pago;
		$mat = (2.01/100)  * $tr->pago;

		$total2 = $seguro + $pension + $cuota_sin + $cuota_imss + $bca + $mat;

		$total3 = $total1 - $total2;
	?>
	<table cellpadding="3" cellspacing="0" border="" width="100%" style="font-size: 8.5px; text-align: center;">
		<tr>
			<td width="5%"><label style="font-size: 8.5px;"></label></td>
			<td width="10%"><label style="font-size: 8.5px;"><?php echo $tr->num_unico?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><?php echo $fin?></label></td>
			<td width="20%"><label style="font-size: 8.5px;"><?php echo $tr->dependencia?></label></td>
			<td width="50%"><label style="font-size: 8.5px;"><?php echo $tr->apellidos.' '.$tr->nombre?></label></td>
		</tr>
		<tr>
			<td width="9%"><label style="font-size: 8.5px;"><br><?php echo $tr->puesto?></label></td>
			<td width="5%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_puesto?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $inicio?></label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $fin?></label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="3%"><label style="font-size: 8.5px;"><br>0</label></td>
			<td width="15%"><label style="font-size: 8.5px;"><br><?php echo $tr->num_seguro?></label></td>
			<td width="32%"><label style="font-size: 8.5px;"><?php echo $tr->curp.'<br>'.$tr->rfc?></label></td>
		</tr>
		<tr>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="10%"><label style="text-align: right; font-size: 8.5px;">1</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">SUELDO</label></td>
						<td width="25%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pago, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">3</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DIA AJUSTE</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo ($extra != 0) ? number_format($extra, 2) : ''?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">4</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P.M.S.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($psm, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">5</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">DESPENSA</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($despensa, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">6</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">ACT. CUL Y DEP.</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($act_cul_dep, 2)?></label></td>
					</tr>
					<?php if($valor != 0):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">12</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">QUINQUINIENTOS (<?php echo $anios?>)</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($valor, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<?php if ($fv[1] == '05' or $fv[1] == 12):?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;">AGUINALDO</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($aguinaldo, 2)?></label></td>
					</tr>
					<?php else:?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<?php endif;?>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total1, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="15%"><label style="text-align: right; font-size: 8.5px;">201</label></td>
						<td width="65%"><label style="text-align: left; font-size: 8.5px;">I.S.S.S.</label></td>
						<td width="20%"><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($seguro, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">202</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">FONDO PENSIONES</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($pension, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">203</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA SINDICAL</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_sin, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">204</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">CUOTA IMSS</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($cuota_imss, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">226</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">L. BCA (DRH) 28/36</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($bca, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;">227</label></td>
						<td><label style="text-align: left; font-size: 8.5px;">P. MAT(DRH) 54/72</label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($mat, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: left; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"></label></td>
						<td><label style="text-align: right; font-size: 8.5px;"><?php echo number_format($total2, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-size: 8.5px;">
					<tr>
						<td><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></td>
					</tr>
					<tr>
						<td><label style="text-align: center; font-size: 8.5px;">$ <?php echo number_format($total3, 2)?></label></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php endif;?>
</body>
</html>