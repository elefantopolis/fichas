      	<div class="starter-template">
        	<h1><?php echo $inicio?></h1>
      	</div>

      	<div class="row">
      		<div class="col-xs-12">
      			<?php if($message):?>
      				<div class="alert alert-success alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $message?>
					</div>
      			<?php endif;?>
      		</div>
      		<div class="col-xs-12">
      			<?php if($error):?>
      				<div class="alert alert-danger alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $error?>
					</div>
      			<?php endif;?>
      		</div>
      	</div>

      	<div class="row">
      		<table class="table table-bordered table-hover" width="100%" id="miTabla">
	      		<thead>
	      			<tr>
		      			<th>Nº</th>
		      			<th>Nombres</th>
		      			<th>Apellidos</th>
						<th>Edad</th>
						<th>Antigüedad</th>
						<th>Acciones</th>
		      		</tr>
	      		</thead>
	      	</table>
      	</div>

		<!-- modal activar -->
      	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modal-activar">
  			<div class="modal-dialog modal-sm" role="document">
    			<div class="modal-content">
    				<?php echo form_open('welcome/activar');?>
      				<div class="modal-body">
      					<input type="hidden" name="id" id="id" value="">
      					¿Estas seguro de activar este registro?
      				</div>
      				<div class="modal-footer">
      					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" value="Activar" class="btn btn-success">
      				</div>
      				<?php echo form_close();?>
    			</div>
  			</div>
		</div>