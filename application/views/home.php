      	<div class="starter-template">
        	<h1><?php echo $inicio?></h1>
      	</div>

      	<div class="row">
      		<div class="col-md-12">
      			<?php if($message):?>
      				<div class="alert alert-success alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $message?>
					</div>
      			<?php endif;?>
      		</div>
      		<div class="col-md-12">
      			<?php if($error):?>
      				<div class="alert alert-danger alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $error?>
					</div>
      			<?php endif;?>
      		</div>
      	</div>

      	<div class="row">
      		<div class="col-md-offset-1 col-md-3">
      			<?php echo anchor('welcome/nuevo', '<i class="glyphicon glyphicon-user"></i> Nuevo', ['class' => 'btn btn-primary btn-block']);?>
      		</div>
          <div class="col-md-offset-1 col-md-3">
            <!--button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#generar"><i class="glyphicon glyphicon-usd"></i> Generar</button-->
          </div>
      	</div>
		<br><br>
      	<div class="row">
      		<table class="table table-bordered table-hover" width="100%" id="miTabla">
	      		<thead>
	      			<tr>
		      			<th>Nº</th>
		      			<th>Nombres</th>
		      			<th>Apellidos</th>
						<th>Edad</th>
						<th>Antigüedad</th>
						<th>Acciones</th>
		      		</tr>
	      		</thead>
	      	</table>
      	</div>

		<!-- modal eliminar -->
      	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modal-eliminar">
  			<div class="modal-dialog modal-sm" role="document">
    			<div class="modal-content">
    				<?php echo form_open('welcome/desactivar');?>
      				<div class="modal-body">
      					<input type="hidden" name="id" id="id" value="">
      					¿Estas seguro de desactivar este registro?
      				</div>
      				<div class="modal-footer">
      					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" value="Desactivar" class="btn btn-danger">
      				</div>
      				<?php echo form_close();?>
    			</div>
  			</div>
		</div>
		
		<!-- modal imprimir -->
		<div class="modal fade" tabindex="-1" role="dialog" id="modal-imprimir">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title">Generar Cheques por empleado.</h4>
      				</div>
      				<?php echo form_open('welcome/generar', ['id' => 'imprimir', 'target' => '_blank', 'name' => 'imprimir']);?>
      				<div class="modal-body">
      					<input type="hidden" name="empleado" id="empleado" value="">
        				<div class="form-group">
        					<label>Seleccione el rango:</label>
        					<div class="input-group">
        						<input type="text" name="inicio" class="form-control required date">
        						<span class="input-group-addon">al</span>
        						<input type="text" name=final class="form-control required date">
        					</div>
        				</div>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        				<button type="submit" class="btn btn-success btn-generar">Generar</button>
      				</div>
      				<?php echo form_close();?>
    			</div>
  			</div>
		</div>

    <div class="modal fade" tabindex="-1" role="dialog" id="generar">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Generar cheques por el listado de empleados.</h4>
          </div>
          <?php echo form_open('welcome/cheques_trabajador');?>
          <div class="modal-body">
            <div class="form-group">
              <label>Rango</label>
              <div class="input-group">
                <input type="text" name="inicio" class="form-control date">
                <span class="input-group-addon"> AL </span>
                <input type="text" name="final" class="form-control date">
              </div>
            </div>
            <!--div class="form-group">
              <label>Tipo de Generacion</label>
              <select name="tipo" id="tipo" class="form-control">
                <option value="01">1ra Quincena de Enero</option>
                <option value="02">2da Quincena de Enero</option>
                <option value="03">1ra Quincena de Febrero</option>
                <option value="04">2da Quincena de Febrero</option>
                <option value="05">1ra Quincena de Marzo</option>
                <option value="06">2da Quincena de Marzo</option>
                <option value="07">1ra Quincena de Abril</option>
                <option value="08">2da Quincena de Abril</option>
                <option value="09">Quincena de Mayo</option>
                <option value="11">1ra Quincena de Junio</option>
                <option value="12">2da Quincena de Junio</option>
                <option value="13">1ra Quincena de Julio</option>
                <option value="14">2da Quincena de Julio</option>
                <option value="15">1ra Quincena de Agosto</option>
                <option value="16">2da Quincena de Agosto</option>
                <option value="17">1ra Quincena de Septiembre</option>
                <option value="18">2da Quincena de Septiembre</option>
                <option value="19">1ra Quincena de Octubre</option>
                <option value="20">2da Quincena de Octubre</option>
                <option value="21">1ra Quincena de Noviembre</option>
                <option value="22">2da Quincena de Noviembre</option>
                <option value="23">Quincena de Diciembre</option>
              </select>
            </div-->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Generar</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->