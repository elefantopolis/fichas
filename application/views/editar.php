		<div class="starter-template">
        	<h1><?php echo $title?></h1>
      	</div>
      	<?php 
      	$fn = explode('-', $tr->nacimiento);
      	$nacimiento = $fn[2].'/'.$fn[1].'/'.$fn[0];
      	$it = explode('-', $tr->inicio_trabajo);
      	$trabajo = $it[2].'/'.$it[1].'/'.$it[0];
      	?>
		<div class="container">
			<div class="row">
      			<?php echo form_open(uri_string(), ['class' => 'form-horizontal', 'id' => 'nuevo']);?>
	      		<div class="col-xs-12">
	      			<div class="form-group">
	      				<label for="num_unico" class="col-xs-2 label-control">Número único empleado:</label>
	      				<div class="col-xs-10">
	      					<input type="text" name="num_unico" id="num_unico" class="form-control required number" value="<?php echo $tr->num_unico?>">
	      				</div>
	      			</div>
	      			<div class="form-group">
	      				<label for="dependencia" class="col-xs-2 label-control">Número Dependencia:</label>
	      				<div class="col-xs-10">
	      					<input type="text" name="dependencia" id="dependencia" class="form-control required number" value="<?php echo $tr->dependencia?>">      					
	      				</div>
	      			</div>
	        		<div class="form-group">
	        			<label for="nombre" class="col-xs-2 label-control">Nombre(s):</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="nombre" id="nombre" class="form-control required uppercase" value="<?php echo $tr->nombre?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="apellidos" class="col-xs-2 label-control">Apellidos:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="apellidos" id="apellidos" class="form-control required uppercase" value="<?php echo $tr->apellidos?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="nacimiento" class="col-xs-2 label-control">Fecha de Nacimiento:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="nacimiento" id="nacimiento" class="form-control required date" value="<?php echo $nacimiento?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="curp" class="col-xs-2 label-control">C.U.R.P.:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="curp" id="curp" class="form-control required uppercase" value="<?php echo $tr->curp?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="rfc" class="col-xs-2 label-control">R.F.C.:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="rfc" id="rfc" class="form-control required uppercase" value="<?php echo $tr->rfc?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="domicilio" class="col-xs-2 label-control">Domicilio:</label>
	        			<div class="col-xs-10">
	        				<textarea name="domicilio" id="domicilio" rows="3" style="resize: none;" class="form-control required uppercase"><?php echo $tr->domicilio?></textarea>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="telefono" class="col-xs-2 label-control">Telefono:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="telefono" id="telefono" class="form-control required phone" value="<?php echo $tr->telefono?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="inicio_trabajo" class="col-xs-2 label-control">Fecha Inicio Laboral:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="inicio_trabajo" id="inicio_trabajo" class="form-control required date" value="<?php echo $trabajo?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="puesto" class="col-xs-2 label-control">Categ. Puesto:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="puesto" id="puesto" class="form-control required uppercase" value="<?php echo $tr->puesto?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="num_puesto" class="col-xs-2 label-control">Número puesto:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="num_puesto" id="num_puesto" class="form-control required number" value="<?php echo $tr->num_puesto?>">
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label for="pago" class="col-xs-2 label-control">Pago Quincenal:</label>
	        			<div class="col-xs-10">
	        				<input type="text" name="pago" id="pago" class="form-control required number" value="<?php echo $tr->pago?>">
	        			</div>
	        		</div>
	      		</div>
	      	</div>

	      	<div class="row">
	      		<div class="col-xs-offset-2 col-xs-3">
	      			<?php echo anchor('', 'Regresar', ['class' => 'btn btn-danger btn-block']);?>
	      		</div>
	      		<div class="col-xs-offset-2 col-xs-3">
	      			<input type="submit" value="Actualizar" class="btn btn-success btn-block">
	      		</div>
	      	</div>
	      	<?php echo form_close();?>
		</div><br><br>