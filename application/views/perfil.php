		<div class="starter-template">
        	<h1><?php echo $title?></h1>
      	</div>

      	<div class="row">
      		<div class="col-md-12">
      			<?php if($message):?>
      				<div class="alert alert-success alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $message?>
					</div>
      			<?php endif;?>
      		</div>
      		<div class="col-md-12">
      			<?php if($error):?>
      				<div class="alert alert-danger alert-dismissible" role="alert">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						<?php echo $error?>
					</div>
      			<?php endif;?>
      		</div>
      	</div>

      	<?php
      	$nac = explode('-', $tr->nacimiento);
		$nacimiento = $nac[2].'/'.$nac[1].'/'.$nac[0];

		$it = explode('-', $tr->inicio_trabajo);
		$inicio_trabajo = $it[2].'/'.$it[1].'/'.$it[0];

		$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->nacimiento));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$edad = $years;

		$diff2 = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
		$years2 = floor($diff2 / (365*60*60*24));
		$months2 = floor(($diff2 - $years * 365*60*60*24) / (30*60*60*24));
		$days2 = floor(($diff2 - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$tiempot = $years2;
      	?>
      	<div class="row">
      		<div class="col-xs-6">
      			<h3>Datos Personales.</h3>
      			<dl class="dl-horizontal">
			  		<dt>Nombre:</dt>
			  		<dd><?php echo $tr->nombre?></dd>
			  		<dt>Apellidos:</dt>
			  		<dd><?php echo $tr->apellidos?></dd>
			  		<dt>Fecha de Nacimiento:</dt>
			  		<dd><?php echo $nacimiento?></dd>
			  		<dt>Edad:</dt>
			  		<dd><?php echo $edad?> años</dd>
			  		<dt>C.U.R.P.</dt>
			  		<dd><?php echo $tr->curp?></dd>
			  		<dt>R.F.C.</dt>
			  		<dd><?php echo $tr->rfc?></dd>
			  		<dt>Domicilio:</dt>
			  		<dd><?php echo $tr->domicilio?></dd>
			  		<dt>Telefono:</dt>
			  		<dd><?php echo $tr->telefono?></dd>
			  		<dt>Fecha de inicio laboral:</dt>
			  		<dd><?php echo $inicio_trabajo?></dd>
			  		<dt>Antigüedad:</dt>
			  		<dd><?php echo $tiempot?> años</dd>
			  		<dt>Puesto y Numero:</dt>
			  		<dd><?php echo $tr->puesto.' - '.$tr->num_puesto?></dd>
			  		<dt>Sueldo:</dt>
			  		<dd>$ <?php echo number_format($tr->pago, 2)?></dd>
			  		<dt>Status:</dt>
			  		<dd><?php echo ($tr->activo == 1) ? 'Activo' : 'Baja'?></dd>
				</dl>
      		</div>
      		<div class="col-xs-6">
      			<h3>Documentos:</h3>
      			<small>Solo archivos PDF</small>
      			<?php if (isset($documentos)):?>
      			<?php foreach ($documentos as $doc => $value):?>
      			<?php
      			switch ($doc) {
      				case 'acta_nacimiento':
      					$nombre_archivo = 'ACTA DE NACIMIENTO';
      					break;

      				case 'curp':
      					$nombre_archivo = 'CURP';
      					break;

      				case 'padres_1':
      					$nombre_archivo = 'ACTA DE NACIMIENTO PADRES 1';
      					break;

      				case 'padres_2':
      					$nombre_archivo = 'ACTA DE NACIMIENTO PADRES 2';
      					break;

      				case 'hijos_1':
      					$nombre_archivo = 'ACTA DE NACIMIENTO HIJOS 1';
      					break;

      				case 'hijos_2':
      					$nombre_archivo = 'ACTA DE NACIMIENTO HIJOS 2';
      					break;

      				case 'hijos_3':
      					$nombre_archivo = 'ACTA DE NACIMIENTO HIJOS 3';
      					break;

      				case 'hijos_4':
      					$nombre_archivo = 'ACTA DE NACIMIENTO HIJOS 4';
      					break;

      				case 'hijos_5':
      					$nombre_archivo = 'ACTA DE NACIMIENTO HIJOS 5';
      					break;

      				case 'comprobante_domicilio':
      					$nombre_archivo = 'COMPROBANTE DE DOMICILIO';
      					break;

      				case 'constancia_origen':
      					$nombre_archivo = 'CONSTANCIA DE ORIGEN Y VECINDAD';
      					break;

      				case 'cartilla_militar':
      					$nombre_archivo = 'CARTILLA MILITAR';
      					break;

      				case 'carta_invitacion':
      					$nombre_archivo = 'CARTA DE INVITACION';
      					break;

      				case 'primaria':
      					$nombre_archivo = 'CERTIFICADO DE PRIMARIA';
      					break;

      				case 'secundaria':
      					$nombre_archivo = 'CERTIFICADO DE SECUNDARIA';
      					break;

      				case 'prepa':
      					$nombre_archivo = 'CERTIFICADO DE BACHILLERATO';
      					break;

      				case 'carrera':
      					$nombre_archivo = 'CERTIFICADO DE LICENCIATURA O INGENIERIA';
      					break;
      				
      				default:
      					$nombre_archivo = '';
      					break;
      			}
      			?>
      			<?php if ($value == ''):?>
      				<div class="row">
      					<div class="col-xs-2 text-right">
      						<button class="btn btn-info btn-subir" title="Subir Archivo" data-archivo="<?php echo $doc?>" data-nombre="<?php echo $nombre_archivo?>"><i class="glyphicon glyphicon-upload"></i></button>
      					</div>
      					<div class="col-xs-9 text-left">
      						<?php echo $nombre_archivo;?>
      					</div>
      				</div>
      			<?php else:?>
      				<div class="row">
      					<div class="col-xs-2 text-right">
      						<button class="btn btn-info btn-cambiar" title="Cambiar Archivo" data-archivo="<?php echo $doc?>" data-nombre="<?php echo $nombre_archivo?>"><i class="glyphicon glyphicon-file"></i></button>
      					</div>
      					<div class="col-xs-9 text-left">
      						<?php echo anchor('site_media/archivos/'.$value, $nombre_archivo, ['target' => '_blank', 'title' => 'Ver Archivo']);?>
      					</div>
      				</div>
      			<?php endif;?>
      			<?php endforeach;?>
      			<?php endif?>
      		</div>
      	</div>

      	<div class="row">
      		<div class="col-xs-offset-2 col-xs-3">
      			<?php echo anchor('', 'Regresar', ['class' => 'btn btn-danger btn-block']);?>
      		</div>
      	</div>

		<!-- Modal Subir archivo -->
      	<div class="modal fade" tabindex="-1" role="dialog" id="modal-subir">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title">SUBIR <span class="nombre"></span></h4>
      				</div>
      				<?php echo form_open_multipart('welcome/subir');?>
      				<div class="modal-body">
      					<input type="hidden" name="trabajador" value="<?php echo $tr->id?>">
      					<input type="hidden" name="tipo" value="" class="campo">
        				<div class="form-group">
	                    	<label class="col-xs-2 label-control">Archivos:</label>
	                    	<div class="col-xs-10">
	                    		<input name="archivo1" id="archivo1" type="file" class="file" data-show-preview="false">
	                    	</div>
	                	</div>
	                	<br><br>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        				<button type="submit" class="btn btn-primary">Cargar Archivo</button>
      				</div>
      				<?php echo form_close();?>
    			</div>
  			</div>
		</div>

		<!-- Modal Cambiar archivo -->
      	<div class="modal fade" tabindex="-1" role="dialog" id="modal-cambiar">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title">CAMBIAR <span class="nombre"></span></h4>
      				</div>
      				<?php echo form_open_multipart('welcome/cambiar');?>
      				<div class="modal-body">
      					<input type="hidden" name="trabajador" value="<?php echo $tr->id?>">
      					<input type="hidden" name="tipo" value="" class="campo">
        				<div class="form-group">
	                    	<label class="col-xs-2 label-control">Archivos:</label>
	                    	<div class="col-xs-10">
	                    		<input name="archivo2" id="archivo2" type="file" class="file" data-show-preview="false">
	                    	</div>
	                	</div>
	                	<br><br>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        				<button type="submit" class="btn btn-primary">Actualizar Archivo</button>
      				</div>
      				<?php echo form_close();?>
    			</div>
  			</div>
		</div>

		<br><br><br>