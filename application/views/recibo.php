<!DOCTYPE>
<html>
<head>
	<title></title>
</head>
<body>
	<?php foreach ($trabajador as $tr):?>
	<?php

	$tipo = '06';
	$dia_extra = 0;

	if ($tipo == '01' || $tipo == '02') {
		$mes = '01';
		$dia_extra = 1;
	}elseif ($tipo == '03' || $tipo == '04') {
		$mes = '02';
	}elseif ($tipo == '05' || $tipo == '06') {
		$mes = '03';
		$dia_extra = 1;
	}elseif ($tipo == '07' || $tipo == '08') {
		$mes = '04';
	}elseif ($tipo == '09' || $tipo == '10') {
		$mes = '05';
		$dia_extra = 1;
	}elseif ($tipo == '11' || $tipo == '12') {
		$mes = '06';
	}elseif ($tipo == '13' || $tipo == '14') {
		$mes = '07';
		$dia_extra = 1;
	}elseif ($tipo == '15' || $tipo == '16') {
		$mes = '08';
		$dia_extra = 1;
	}elseif ($tipo == '17' || $tipo == '18') {
		$mes = '09';
	}elseif ($tipo == '19' || $tipo == '20') {
		$mes = '10';
		$dia_extra = 1;
	}elseif ($tipo == '21' || $tipo == '22') {
		$mes = '11';
	}else{
		$mes = '12';
		$dia_extra = 1;
	}

	if ($tipo == '01' || $tipo == '03' || $tipo == '05' || $tipo == '07' || $tipo == '09' || $tipo == '11' || $tipo == '13' || $tipo == '15' || $tipo == '17' || $tipo == '19' || $tipo == '21' || $tipo == '23') {
		$fecha = date('Y').'/'.$mes.'/01';
		$_fecha = strtotime($fecha);
		$dt = new DateTime($fecha);

		$end = $dt->modify( '+14 day' ); 

		$final =  $end->format('Y/m/d');

	}elseif ($tipo == '02' || $tipo == '04' || $tipo == '06' || $tipo == '08' || $tipo == 10 || $tipo == '12' || $tipo == '14' || $tipo == '16' || $tipo == '18' || $tipo == '20' || $tipo == '22' || $tipo == '24') {
		$fecha = date('Y').'/'.$mes.'/16';
		$_fecha = strtotime($fecha);
		$dt = new DateTime($fecha);

		$end = $dt->modify('last day of this month');

		$final =  $end->format('Y/m/d');
	}else{
		$this->session->set_flashdata('error', 'Algun dato esta mal...');
		redirect('','refresh');
	}

	$validacion = strtotime($fecha);
	$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
	$years = floor($diff / (365*60*60*24));
	$anios = round($years/5);	

	//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		

	if ($dia_extra == 1 and $end->format('d') > 15) {
		$extra = $tr->pago/15;
	}else{
		$extra = 0;
	}

	$psm = (18.18/100) * $tr->pago;
	$despensa = (7.41/100) * $tr->pago;
	$act_cul_dep = (6.193/100) * $tr->pago;
	$quiniquientos = (10.173/100) * $tr->pago;

	if ($anios != 0) {
		$valor = $anios * $quiniquientos;
	}else{
		$valor = 0;
	}

	$total1 = $tr->pago + $extra + $psm + $despensa + $act_cul_dep + $valor;

	//Deducciones

	$seguro = (13.14/100) * $tr->pago;
	$pension = (9/100) * $tr->pago;
	$cuota_sin = (1/100) * $tr->pago;
	$cuota_imss = (4.12/100) * $tr->pago;
	$bca = (2.24/100) * $tr->pago;
	$mat = (2.01/100)  * $tr->pago;

	$total2 = $seguro + $pension + $cuota_sin + $cuota_imss + $bca + $mat;
	?>
	<br>
	<table cellpadding="3" cellspacing="0" border="1" width="100%" style="font-size: 4px; text-align: center;">
		<tr>
			<td width="10%">FOLIO</td>
			<td width="15%">No. UNICO EMPLEADO</td>
			<td width="15%">FECHA DE PAGO</td>
			<td width="20%">DEPENDENCIA</td>
			<td width="40%">NOMBRE</td>
		</tr>
		<tr>
			<td><label style="font-size: 8px;"></label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->num_unico?></label></td>
			<td><label style="font-size: 8px;"><?php echo $end->format("d").' de '.$end->format("M").' del '.$end->format("Y")?></label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->dependencia?></label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->apellidos.' '.$tr->nombre?></label></td>
		</tr>
		<tr>
			<td width="15%" colspan="2">PLAZA</td>
			<td width="30%" colspan="2">PERIODO DE PAGO</td>
			<td width="5%" rowspan="2">FALTAS</td>
			<td width="5%" rowspan="2">RETAR</td>
			<td width="5%" rowspan="2">HRS. EXT.</td>
			<td width="10%" rowspan="2">AFILIACION IMSS</td>
			<td width="30%" rowspan="2">CLAVE UNICA DE REGISTRO DE POBLACION</td>
		</tr>
		<tr>
			<td>CATEG. PUESTO</td>
			<td>NUMERO</td>
			<td>DESDE</td>
			<td>HASTA</td>
		</tr>
		<tr>
			<td><label style="font-size: 8px;"><?php echo $tr->puesto?></label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->num_puesto?></label></td>
			<td><label style="font-size: 8px;"><?php echo date('d', $_fecha).' de '.date('M', $_fecha).' del '.date('Y', $_fecha)?></label></td>
			<td><label style="font-size: 8px;"><?php echo $end->format("d").' de '.$end->format("M").' del '.$end->format("Y")?></label></td>
			<td><label style="font-size: 8px;">0</label></td>
			<td><label style="font-size: 8px;">0</label></td>
			<td><label style="font-size: 8px;">0</label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->num_seguro?></label></td>
			<td><label style="font-size: 8px;"><?php echo $tr->curp.'<br>'.$tr->rfc?></label></td>
		</tr>
		<tr>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="1" style="font-size: 4px;">
					<tr>
						<td colspan="3"><label style="text-align: center;">PERCEPCIONES</label></td>
					</tr>
					<tr>
						<td width="10%"><label style="text-align: center;">CLAVE</label></td>
						<td width="60%"><label style="text-align: center;">CONCEPTO</label></td>
						<td width="30%"><label style="text-align: center;">IMPORTE</label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">1</label></td>
						<td><label style="text-align: left; font-size: 8px;">SUELDO</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($tr->pago, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">3</label></td>
						<td><label style="text-align: left; font-size: 8px;">DIA AJUSTE</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($extra, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">4</label></td>
						<td><label style="text-align: left; font-size: 8px;">P.M.S.</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($psm, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">5</label></td>
						<td><label style="text-align: left; font-size: 8px;">DESPENSA</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($despensa, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">6</label></td>
						<td><label style="text-align: left; font-size: 8px;">ACT. CUL Y DEP.</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($act_cul_dep, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">12</label></td>
						<td><label style="text-align: left; font-size: 8px;">QUINQUINIENTOS (<?php echo $anios?>)</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($valor, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;"></label></td>
						<td><label style="text-align: right; font-size: 8px;">TOTAL PERCEPSIONES $</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($total1, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="40%">
				<table width="100%" cellspacing="0" cellpadding="3" border="1" style="font-size: 4px;">
					<tr>
						<td colspan="3"><label style="text-align: center;">DEDUCCIONES</label></td>
					</tr>
					<tr>
						<td width="15%"><label style="text-align: center;">CLAVE</label></td>
						<td width="55%"><label style="text-align: center;">CONCEPTO</label></td>
						<td width="30%"><label style="text-align: center;">IMPORTE</label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">201</label></td>
						<td><label style="text-align: left; font-size: 8px;">I.S.S.S.</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($seguro, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">202</label></td>
						<td><label style="text-align: left; font-size: 8px;">FONDO PENSIONES</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($pension, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">203</label></td>
						<td><label style="text-align: left; font-size: 8px;">CUOTA SINDICAL</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($cuota_sin, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">204</label></td>
						<td><label style="text-align: left; font-size: 8px;">CUOTA IMSS</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($cuota_imss, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">226</label></td>
						<td><label style="text-align: left; font-size: 8px;">L. BCA (DRH) 28/36</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($bca, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;">227</label></td>
						<td><label style="text-align: left; font-size: 8px;">P. MAT(DRH) 54/72</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($mat, 2)?></label></td>
					</tr>
					<tr>
						<td><label style="text-align: right; font-size: 8px;"></label></td>
						<td><label style="text-align: right; font-size: 8px;">TOTAL DEDUCCIONES $</label></td>
						<td><label style="text-align: right; font-size: 8px;"><?php echo number_format($total2, 2)?></label></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%" cellspacing="0" cellpadding="3" border="1" style="font-size: 5px;">
					<tr>
						<td><label style="text-align: justify;">RECIBI DEL GOBIERNO DEL ESTADO DE OAXACA; LA CANTIDAD QUE AMPARA ESTE RECIBO, CORRESPONDIENTE A LAS PERCEPCIONES A QUE TENGO DERECHO EN EL PERIODO QUE SE INDICA. <br>MANIFIESTO ASI MISMO MI CONFORMIDAD A LAS DEDUCCIONES QUE ME HAN SIDO EFECTUADAS.</label></td>
					</tr>
					<tr>
						<td><label style="text-align: justify;">PARA CUALQUIER ACLARACION PRESENTAR ESTE COMPROBANTE AL DEPTO DE SALARIOS Y PRESTACIONES.</label></td>
					</tr>
					<tr>
						<td><label style="text-align: center;">NETO</label></td>
					</tr>
					<tr>
						<td><label style="text-align: center; font-size: 8px;">$ <?php echo number_format(($total1 - $total2), 2)?></label></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p></p><p></p>
	<?php endforeach;?>
</body>
</html>