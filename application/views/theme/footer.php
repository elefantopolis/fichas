    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>site_media/js/jquery.js"></script>
    <script src="<?php echo base_url()?>site_media/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>site_media/js/dataTables.js"></script>
	<script>
		var base_url = '<?php echo base_url()?>';
	</script>

    <?php 
    if (isset($js)) {
    	foreach ($js as $script) {
    		echo '<script src="'.base_url().'site_media/js/'.$script.'.js"></script>';
    	}
    }
    ?>
  </body>
</html>