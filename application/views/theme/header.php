<!DOCTYPE>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Control de pagos">
    <meta name="author" content="@lord_fire">
    <link rel="icon" href="<?php echo base_url()?>site_media/img/fire-vector.png">

    <title>..:: Fichas ::..</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>site_media/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>site_media/css/home.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>site_media/css/dataTables.css">

    <?php
    if(isset($css)){
      foreach($css as $link){
        echo '<link rel="stylesheet" href="'.base_url().'site_media/css/'.$link.'.css">';
      }
    }
    ?>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url()?>">Control de Trabajadores</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li <?php echo ($link1 == 1) ? 'class="active"' : ''?>><a href="<?php echo base_url()?>">Inicio</a></li>
            <li <?php echo ($link2 == 1) ? 'class="active"' : ''?>><a href="<?php echo base_url()?>welcome/eliminados">Eliminados</a></li>
            <!--li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">