<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insertar($table, $datos)
	{
		$this->db->insert($table, $datos);
		return $this->db->insert_id();
	}

	public function seleccionar($table, $campos='', $where=array(), $limit='')
    {
    	if (isset($campos)) {
    		$this->db->select($campos);
    	}
        if (isset($where) && is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() === 0 ) {
            return FALSE;
        }else{
            if ( !empty($limit) && is_numeric($limit) && $limit == 1)
            {
                return $query->row();
            }else{
            	return $query->result();
            }
        }
    }

    public function actualizar($table, $data, $where)
	{
		$this->db->where($where);
		$update = $this->db->update($table, $data); 

		return $update;
	}

}

/* End of file My_model.php */
/* Location: ./application/models/My_model.php */
?>