<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('my_model', 'mymodel');
	}

	public function trabajadores()
	{
		$trabajadores = $this->mymodel->seleccionar('trabajador', '', ['activo' => 1]);

		$data = [];
		$num = 1;
		if ($trabajadores) {
			foreach ($trabajadores as $tr) {

				$nac = explode('-', $tr->nacimiento);
				$nacimiento = $nac[2].'/'.$nac[1].'/'.$nac[0];

				$it = explode('-', $tr->inicio_trabajo);
				$inicio_trabajo = $it[2].'/'.$it[1].'/'.$it[0];

				$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->nacimiento));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
				$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

				$edad = $years;

				$diff2 = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
				$years2 = floor($diff2 / (365*60*60*24));
				$months2 = floor(($diff2 - $years * 365*60*60*24) / (30*60*60*24));
				$days2 = floor(($diff2 - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

				$tiempot = $years2;

				//$botones = '<a href="'.base_url().'welcome/editar/'.$tr->id.'" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i></a> <button class="btn btn-danger btn-eliminar" data-id="'.$tr->id.'"><i class="glyphicon glyphicon-trash"></i></button> <button class="btn btn-primary btn-imprimir" data-id="'.$tr->id.'"><i class="glyphicon glyphicon-print"></i></button>';
				$botones = '<a href="'.base_url().'welcome/editar/'.$tr->id.'" title="Editar" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i></a> <button class="btn btn-danger btn-eliminar" title="Desactivar" data-id="'.$tr->id.'"><i class="glyphicon glyphicon-trash"></i></button> <a href="'.base_url().'welcome/perfil/'.$tr->id.'" title="Perfil" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i></a> <button class="btn btn-success btn-imprimir" data-id="'.$tr->id.'"><i class="glyphicon glyphicon-print"></i></button>';

				$data[] = ['num' => $num, 'nombre' => $tr->nombre, 'apellidos' => $tr->apellidos, 'edad' => $edad, 'antiguedad' => $tiempot.' años', 'botones' => $botones];

				$num++;
			}
		}
		

		$result = ['draw' => 1, 'recordsTotal' => count($data), 'recordsFiltered' => count($data), 'data' => $data];

		$this->output->set_output(json_encode($result));
	}

	public function trabajadores_eliminados()
	{
		$trabajadores = $this->mymodel->seleccionar('trabajador', '', ['activo' => 0]);

		$data = [];
		$num = 1;
		if ($trabajadores) {
			foreach ($trabajadores as $tr) {

				$fn = explode('-', $tr->nacimiento);
				$dia = date('d');
				$mes = date('m');
				$anio = date('Y');

				if ($mes == $fn[1] and $fn[2] > $dia) {
					$anio = ($anio-1);
				}

				if ($fn[1] > $mes) {
					$anio = ($anio-1);
				}
				$edad = $anio - $fn[0];

				$it = explode('-', $tr->inicio_trabajo);

				if ($mes == $it[1] and $it[2] > $dia) {
					$anio = ($anio-1);
				}
				if ($it[1] > $mes) {
					$anio = ($anio-1);
				}

				$tiempot = $anio - $it[0];

				$botones = '<button class="btn btn-primary btn-activar" data-id="'.$tr->id.'"><i class="glyphicon glyphicon-check"></i></button>';

				$data[] = ['num' => $num, 'nombre' => $tr->nombre, 'apellidos' => $tr->apellidos, 'edad' => $edad, 'antiguedad' => $tiempot, 'botones' => $botones];

				$num++;
			}
		}
		

		$result = ['draw' => 1, 'recordsTotal' => count($data), 'recordsFiltered' => count($data), 'data' => $data];

		$this->output->set_output(json_encode($result));
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */

?>