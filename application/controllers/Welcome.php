<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->model('my_model', 'mymodel');
		$this->load->library('session');
	}
	public function index()
	{
		$data['js'] = ['inicio/home', 'bootstrap-datepicker', 'jquery.maskedinput', 'validate'];
		$data['css'] = ['bootstrap-datepicker', 'validate'];
		$data['message'] = $this->session->flashdata('message');
		$data['error'] = $this->session->flashdata('error');
		$data['link1'] = 1;
		$data['link2'] = 0;
		$data['inicio'] = 'Bienvenido.';
		render_vista('home', $data);
	}

	public function nuevo()
	{
		$this->form_validation->set_rules('num_unico', 'Número Único', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$data['js'] = ['inicio/nuevo', 'validate', 'bootstrap-datepicker', 'jquery.maskedinput'];
			$data['css'] = ['bootstrap-datepicker', 'validate'];
			$data['title'] = 'Registrar un Nuevo Trabajador.';
			$data['link1'] = 0;
			$data['link2'] = 0;

			render_vista('nuevo', $data);

		} else {
			$post = $this->input->post(NULL, TRUE);
			$post['activo'] = 1;

			$nacimiento = explode('/', $post['nacimiento']);
			$post['nacimiento'] = $nacimiento[2].'/'.$nacimiento[1].'/'.$nacimiento[0];

			$inicio_trabajo = explode('/', $post['inicio_trabajo']);
			$post['inicio_trabajo'] = $inicio_trabajo[2].'/'.$inicio_trabajo[1].'/'.$inicio_trabajo[0];

			$id = $this->mymodel->insertar('trabajador', $post);

			$docs = [
				'trabajador' => $id,
				'acta_nacimiento' => '',
				'curp' => '',
				'padres_1' => '',
				'padres_2' => '',
				'hijos_1' => '',
				'hijos_2' => '',
				'hijos_3' => '',
				'hijos_4' => '',
				'hijos_5' => '',
				'comprobante_domicilio' => '',
				'constancia_origen' => '',
				'cartilla_militar' => '',
				'carta_invitacion' => '',
				'primaria' => '',
				'secundaria' => '',
				'prepa' => '',
				'carrera' => ''
			];

			$this->mymodel->insertar('documentos', $docs);

			if ($id) {
				$this->session->set_flashdata('message', 'Trabajador guardado correctamente.');
				redirect('','refresh');
			}else{
				$this->session->set_flashdata('error', 'Error al insertar, intente otra vez.');
				redirect('','refresh');
			}
		}
	}

	public function perfil($id='')
	{
		if ($id == '') {
			$this->session->set_flashdata('error', 'Faltan datos!');
			redirect('','refresh');
		}

		$data['js'] = ['inicio/perfil', 'fileinput'];
		$data['css'] = ['fileinput'];
		$data['message'] = $this->session->flashdata('message');
		$data['error'] = $this->session->flashdata('error');
		$data['title'] = 'Perfil del Trabajador.';
		$data['link1'] = 0;
		$data['link2'] = 0;
		$data['tr'] = $this->mymodel->seleccionar('trabajador', '', ['id' => $id], 1);
		$data['documentos'] = $this->mymodel->seleccionar('documentos', 'acta_nacimiento, curp, padres_1, padres_2, hijos_1, hijos_2, hijos_3, hijos_4, hijos_5, comprobante_domicilio, constancia_origen, cartilla_militar, carta_invitacion, primaria, secundaria, prepa, carrera', ['trabajador' => $id], 1);
		render_vista('perfil', $data);
	}

	public function editar($id='')
	{
		if ($id == '') {
			$this->session->set_flashdata('error', 'Faltan datos!');
			redirect('','refresh');
		}

		$this->form_validation->set_rules('num_unico', 'Número Único', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$data['js'] = ['inicio/editar', 'validate', 'bootstrap-datepicker', 'jquery.maskedinput'];
			$data['css'] = ['bootstrap-datepicker', 'validate'];
			$data['title'] = 'Editar Trabajador.';
			$data['link1'] = 0;
			$data['link2'] = 0;

			$data['tr'] = $this->mymodel->seleccionar('trabajador', '', ['id' => $id], 1);
			render_vista('editar', $data);

		} else {
			$post = $this->input->post(NULL, TRUE);

			$nacimiento = explode('/', $post['nacimiento']);
			$post['nacimiento'] = $nacimiento[2].'/'.$nacimiento[1].'/'.$nacimiento[0];

			$inicio_trabajo = explode('/', $post['inicio_trabajo']);
			$post['inicio_trabajo'] = $inicio_trabajo[2].'/'.$inicio_trabajo[1].'/'.$inicio_trabajo[0];

			$up = $this->mymodel->actualizar('trabajador', $post, ['id' =>  $id]);

			if ($id) {
				$this->session->set_flashdata('message', 'Trabajador actualizado correctamente.');
				redirect('','refresh');
			}else{
				$this->session->set_flashdata('error', 'Error al actualizar, intente otra vez.');
				redirect('','refresh');
			}
		}
	}

	public function desactivar()
	{
		$post = $this->input->post(NULL, TRUE);

		$up = $this->mymodel->actualizar('trabajador', ['activo' => 0], ['id' => $post['id']]);

		if ($up) {
			$this->session->set_flashdata('message', 'Trabajador desactivado correctamente.');
			redirect('','refresh');
		}else{
			$this->session->set_flashdata('error', 'Error al desactivar, intente otra vez.');
			redirect('','refresh');
		}/**/
	}

	public function eliminados()
	{
		$data['js'] = ['inicio/eliminados'];
		$data['message'] = $this->session->flashdata('message');
		$data['link1'] = 0;
		$data['link2'] = 1;
		$data['error'] = $this->session->flashdata('error');
		$data['inicio'] = 'Trabajadores eliminados.';
		render_vista('eliminados', $data);
	}

	public function activar()
	{
		$post = $this->input->post(NULL, TRUE);

		$up = $this->mymodel->actualizar('trabajador', ['activo' => 1], ['id' => $post['id']]);

		if ($up) {
			$this->session->set_flashdata('message', 'Trabajador activado correctamente.');
			redirect('','refresh');
		}else{
			$this->session->set_flashdata('error', 'Error al activar, intente otra vez.');
			redirect('','refresh');
		}
	}

	public function subir()
	{
		$post = $this->input->post(NULL, TRUE);
		$campo = $post['tipo'];
		$tra = $post['trabajador'];
		unset($post['tipo']); unset($post['trabajador']); unset($post['archivo1']);

		$logo = $this->upload('archivo1');

		if ($logo['success'] == '') {
            $this->session->set_flashdata('error', $logo['error']);
            redirect('welcome/perfil/'.$tra,'refresh');
        }else{
            $post[$campo] = $logo['success']['file_name'];
            $up = $this->mymodel->actualizar('documentos', $post, ['trabajador' => $tra]);
            if ($up) {
                $this->session->set_flashdata('message', 'El documento se subió con éxito.');
                redirect('welcome/perfil/'.$tra,'refresh');
            }else{
                $this->session->set_flashdata('error', 'Error, intente otra vez.');
                redirect('welcome/perfil/'.$tra,'refresh');
            }
        }
	}

	public function cambiar()
	{
		$post = $this->input->post(NULL, TRUE);
		$campo = $post['tipo'];
		$tra = $post['trabajador'];
		unset($post['tipo']); unset($post['trabajador']); unset($post['archivo2']);

		$old_file = $this->mymodel->seleccionar('documentos', [$campo], ['trabajador' => $tra], 1);

		$logo = $this->upload('archivo2');

		if ($logo['success'] == '') {
            $this->session->set_flashdata('error', $logo['error']);
            redirect('welcome/perfil/'.$tra,'refresh');
        }else{
        	unlink('./site_media/archivos/'.$old_file->$campo);
            $post[$campo] = $logo['success']['file_name'];
            $up = $this->mymodel->actualizar('documentos', $post, ['trabajador' => $tra]);
            if ($up) {
                $this->session->set_flashdata('message', 'El documento se subió con éxito.');
                redirect('welcome/perfil/'.$tra,'refresh');
            }else{
                $this->session->set_flashdata('error', 'Error, intente otra vez.');
                redirect('welcome/perfil/'.$tra,'refresh');
            }
        }
	}

	private function upload($file)
    {
        $config['upload_path'] = './site_media/archivos/';
        $config['allowed_types'] = 'PDF|pdf';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        
        if ( ! $this->upload->do_upload($file)){
            $dato['error'] = $this->upload->display_errors();
            $dato['success'] = '';
            return $dato;
        }
        else{
            $dato['success'] = $this->upload->data();
            $dato['error'] = '';
            return $dato;
        }
    }

	/*public function generar()
	{
		$post = $this->input->post(NULL, TRUE);

		$inicio = explode('/', $post['inicio']);
		$inicio2 = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];

		$final = explode('/', $post['final']);
		$final2 = $final[2].'-'.$final[1].'-'.$final[0];

		$start = strtotime($inicio2);
		$end = strtotime($final2);

		$periodos = [];
		while ($start < $end) {
			$fecha = date('d/m/Y', $start);
			$fecha2 = explode('/', $fecha);
			$begin = new DateTime($fecha2[2].'/'.$fecha2[1].'/'.$fecha2[0]);

			if ($fecha2[0] < 15) {
				$dt = $begin->modify('+ 14 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => $fecha2[0].'/'.$fecha2[1].'/'.$fecha2[2], 'fin' => $dt->format('d/m/Y')];

			}elseif ($fecha2[0] == 30 and $fecha2[1] == '04') {
				$dt = $begin->modify('+ 31 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}elseif ($fecha2[0] == 30 and $fecha2[1] == 11) {
				$dt = $begin->modify('+ 31 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}elseif($fecha2[0] == 31 or $fecha2[0] == 28 or $fecha2[0] == 29 or $fecha2[0] == 30){
				$dt = $begin->modify('+ 15 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}else{
				$dt = $begin->modify( 'last day of this month' );
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '16/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}

		}
		$data['fechas'] = $periodos;

		$data['tr'] = $this->mymodel->seleccionar('trabajador', '', ['activo' => 1, 'id' => $post['empleado']], 1);
		$this->load->library(array('pdf'));

		$pdf = new Pdf();
        $pdf->setCreator(PDF_CREATOR);
        $pdf->SetTitle('Recibos');
        $pdf->SetAuthor('@lordfire');
        $pdf->SetDisplayMode('real', 'default');

        $pdf->setPrintHeader(FALSE);
        $pdf->setPrintFooter(FALSE);
        $pdf->SetAutoPageBreak(TRUE, 0);

        $pdf->AddPage('P', 'A4');

        $pdf->SetY(5);
        $pdf->SetX(5);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('times', '', 10);
        $html = $this->load->view('recibo2', $data, true);

        $pdf->writeHTML($html);

        $pdf->Output('Cheques del '.$post['inicio'].' al '.$post['final'].'.pdf', 'I');
	}/**/

	public function generar()
	{
		$post = $this->input->post(NULL, TRUE);

		$inicio = explode('/', $post['inicio']);
		$inicio2 = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];

		$final = explode('/', $post['final']);
		$final2 = $final[2].'-'.$final[1].'-'.$final[0];

		$start = strtotime($inicio2);
		$end = strtotime($final2);

		$periodos = [];
		while ($start < $end) {
			$fecha = date('d/m/Y', $start);
			$fecha2 = explode('/', $fecha);
			$begin = new DateTime($fecha2[2].'/'.$fecha2[1].'/'.$fecha2[0]);

			if ($fecha2[0] == '01') {
				$dt = $begin->modify('+ 14 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => $fecha2[0].'/'.$fecha2[1].'/'.$fecha2[2], 'fin' => $dt->format('d/m/Y')];

			}elseif ($fecha2[0]>1 and $fecha2[0]<15) {
				$start = strtotime( $fecha2[2].'/'.$fecha2[1].'/15');

				$periodos[] = ['inicio' => $fecha2[0].'/'.$fecha2[1].'/'.$fecha2[2], 'fin' => '15/'.$fecha2[1].'/'.$fecha2[2]];
			} elseif ($fecha2[0] == 30 and $fecha2[1] == '04') {
				$dt = $begin->modify('+ 31 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}elseif ($fecha2[0] == 30 and $fecha2[1] == 11) {
				$dt = $begin->modify('+ 31 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}elseif($fecha2[0] == 31 or $fecha2[0] == 28 or $fecha2[0] == 29 or $fecha2[0] == 30){
				$dt = $begin->modify('+ 15 day');
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '01/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}else{
				$dt = $begin->modify( 'last day of this month' );
				$start = strtotime( $dt->format('Y/m/d') );

				$periodos[] = ['inicio' => '16/'.$dt->format('m').'/'.$dt->format('Y'), 'fin' => $dt->format('d/m/Y')];

			}

		}
		$data['fechas'] = $periodos;

		$data['tr'] = $this->mymodel->seleccionar('trabajador', '', ['activo' => 1, 'id' => $post['empleado']], 1);

		$total_unidades = count($data['fechas']);
        $total_hojas = $total_unidades / 3;
        //echo $total_hojas; die();
        if (is_float($total_hojas)) {
            $hojas = (int)$total_hojas+1;
        }else{
            $hojas = $total_hojas;
        }
        
		$this->load->library(array('pdf'));

		$pdf = new Pdf();
        $pdf->setCreator(PDF_CREATOR);
        $pdf->SetTitle('Recibos');
        $pdf->SetAuthor('@lordfire');
        $pdf->SetDisplayMode('real', 'default');

        $pdf->setPrintHeader(FALSE);
        $pdf->setPrintFooter(FALSE);
        $pdf->SetAutoPageBreak(FALSE, 0);
        $pdf->setMargins(0,0,0,0);

        $pdfWidth= "217";
		$pdfHeight= "280";
		$resolution= array($pdfWidth, $pdfHeight);/**/

        $num = 1;
        for ($i=0; $i < $hojas; $i++) { 

        	$data['hoja'] = $num;
	        $pdf->AddPage('P', $resolution, $keepmargins=true);
	        //$pdf->AddPage('P', 'A4');

	        $pdf->SetY(0);
            $pdf->SetX(0);
	        $html = $this->load->view('recibo3', $data, true);
	        $pdf->writeHTML($html);
	        $num++;
    	}

        $pdf->Output('Cheques del '.$post['inicio'].' al '.$post['final'].'.pdf', 'I');
	}/**/

	/*public function all_cheques()
	{
		$post = $this->input->post(NULL, TRUE);
		$tipo = $post['tipo'];

		$trabajadores = $this->mymodel->seleccionar('trabajador', '', ['activo' => 1]);

		$dia_extra = 0;

		if ($tipo == '01' || $tipo == '02') {
			$mes = '01';
			$dia_extra = 1;
		}elseif ($tipo == '03' || $tipo == '04') {
			$mes = '02';
		}elseif ($tipo == '05' || $tipo == '06') {
			$mes = '03';
			$dia_extra = 1;
		}elseif ($tipo == '07' || $tipo == '08') {
			$mes = '04';
		}elseif ($tipo == '09' || $tipo == '10') {
			$mes = '05';
			$dia_extra = 1;
		}elseif ($tipo == '11' || $tipo == '12') {
			$mes = '06';
		}elseif ($tipo == '13' || $tipo == '14') {
			$mes = '07';
			$dia_extra = 1;
		}elseif ($tipo == '15' || $tipo == '16') {
			$mes = '08';
			$dia_extra = 1;
		}elseif ($tipo == '17' || $tipo == '18') {
			$mes = '09';
		}elseif ($tipo == '19' || $tipo == '20') {
			$mes = '10';
			$dia_extra = 1;
		}elseif ($tipo == '21' || $tipo == '22') {
			$mes = '11';
		}else{
			$mes = '12';
			$dia_extra = 1;
		}

		if ($tipo == '01' || $tipo == '03' || $tipo == '05' || $tipo == '07' || $tipo == '09' || $tipo == '11' || $tipo == '13' || $tipo == '15' || $tipo == '17' || $tipo == '19' || $tipo == '21' || $tipo == '23') {
			$fecha = date('Y').'/'.$mes.'/01';
			$dt = new DateTime($fecha);

			$end = $dt->modify( '+14 day' ); 

			$final =  $end->format('Y/m/d');

		}elseif ($tipo == '02' || $tipo == '04' || $tipo == '06' || $tipo == '08' || $tipo == 10 || $tipo == '12' || $tipo == '14' || $tipo == '16' || $tipo == '18' || $tipo == '20' || $tipo == '22' || $tipo == '24') {
			$fecha = date('Y').'/'.$mes.'/16';
			$dt = new DateTime($fecha);

			$end = $dt->modify('last day of this month');

			$final =  $end->format('Y/m/d');
		}else{
			$this->session->set_flashdata('error', 'Algun dato esta mal...');
			redirect('','refresh');
		}

		$validacion = strtotime($fecha);
		$datos = [];
		$other = [];
		$num = 1;
		foreach ($trabajadores as $tr) {

			//percepciones
			$diff = abs(strtotime(date('Y-m-d')) - strtotime($tr->inicio_trabajo));
			$years = floor($diff / (365*60*60*24));
			$anios = round($years/5);	

			//echo $years.' - '.$anios.' - '.$tr->inicio_trabajo.'<br>';		

			if ($dia_extra == 1) {
				$extra = $tr->pago/15;
			}else{
				$extra = 0;
			}

			$psm = 18.18/100;
			$despensa = 7.41/100;
			$act_cul_dep = 6.19/100;
			$quiniquientos = 10.17/100;

			if ($anios != 0) {
				$valor = $anios * ($quiniquientos * $tr->pago);
			}else{
				$valor = 0;
			}

			//Deducciones

			$seguro = 13.14/100;
			$pension = 9/100;
			$cuota_sin = 1/100;
			$cuota_imss = 4.12/100;
			$bca = 2.24/100;
			$mat = 2.01/100;

			$it = strtotime($tr->inicio_trabajo);
			if ($validacion > $it) {
				$datos[] = [
					'folio' => $num,
					'cliente' => $tr->id,
					'monto' => $tr->pago,
					'dia_ajuste' => $extra,
					'psm' =>  $tr->pago * $psm,
					'despensa' =>  $tr->pago * $despensa,
					'act_cul_dep' =>  $tr->pago * $act_cul_dep,
					'quiniquientos' => $valor,
					'isss' => $tr->pago * $seguro,
					'pension' => $tr->pago * $pension,
					'sindcato' => $tr->pago * $cuota_sin,
					'imss' => $tr->pago * $cuota_imss,
					'bca' => $tr->pago * $bca,
					'mat' => $tr->pago * $mat

				];
			}
			$num++;
		}
		
		echo "<pre>"; print_r($datos); die();
	}

	/*public function cheques_trabajador()
	{
		$post = $this->input->post(NULL, TRUE);

		$array_inicio = explode('/', $post['inicio']);
		$array_final = explode('/', $post['final']);

		$inicio = strtotime($array_inicio[2].'/'.$array_inicio[1].'/'.$array_inicio[0]);
		$fin = strtotime($array_final[2].'/'.$array_final[1].'/'.$array_final[0]);

		$begin = new DateTime($array_inicio[2].'/'.$array_inicio[1].'/'.$array_inicio[0]);
		//$end = new DateTime($array_final[2].'/'.$array_final[1].'/'.$array_final[0]);
		echo $inicio.' - '.$fin;
		while ($inicio < $fin) {
			$inicio = strtotime ( '+14 day' , $inicio ) ;
			echo date('d-m-Y', $inicio)."<br>";
		}



		/*$interval = new DateInterval('P15D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		foreach($daterange as $date){
		    echo $date->format("Y-m-d") . "<br>";
		}*/

		//$calcula = $this->calcula_tiempo($post['inicio'], $post['final']);

		//echo $calcula;
	//}

	public function calcula_tiempo($value, $row)
	{
      $fecha_nacimiento = $value;
      $fecha_actual = $row;
      $array_nacimiento = explode ( "/", $fecha_nacimiento ); 
      $array_actual = explode ( "/", $fecha_actual ); 
      $anos =  $array_actual[2] - $array_nacimiento[2]; 
      $meses = $array_actual[1] - $array_nacimiento[1]; 
      $dias =  $array_actual[0] - $array_nacimiento[0]; 
      if ($dias < 0) 
        { 
          	$meses; 
            switch ($array_actual[1]) 
            { 
              case 1: 
                $dias_mes_anterior=31;
                break; 
              case 2:     
                $dias_mes_anterior=31;
                break; 
              case 3:  
                if (bisiesto($array_actual[2])) 
                  { 
                    $dias_mes_anterior=29;
                    break; 
                  } 
                else 
                  { 
                    $dias_mes_anterior=28;
                    break; 
                  } 
              case 4:
                $dias_mes_anterior=31;
                break; 
              case 5:
                $dias_mes_anterior=30;
                break; 
              case 6:
                $dias_mes_anterior=31;
                break; 
              case 7:
                $dias_mes_anterior=30;
                break; 
              case 8:
                $dias_mes_anterior=31;
                break; 
              case 9:
                $dias_mes_anterior=31;
                break; 
              case 10:
                $dias_mes_anterior=30;
                break; 
              case 11:
                $dias_mes_anterior=31;
                break; 
              case 12:
                $dias_mes_anterior=30;
                break; 
            } 
          $dias=$dias + $dias_mes_anterior;
            if ($dias < 0)
              {
                $meses;
                if($dias == -1)
                  {
                    $dias = 30;
                  }
                if($dias == -2)
                  {
                    $dias = 29;
                  }
              }
        }
        if ($meses < 0) 
          { 
            $anos; 
            $meses=$meses + 12; 
          }
        $tiempo[0] = $anos;
        $tiempo[1] = $meses;
        $tiempo[2] = $dias;
        $bisiesto=false; 
        if (checkdate(2,29,date('Y'))) 
          { 
            $bisiesto=true; 
          }
        if ($meses != 1) 
        {
          return $tiempo = $anos.' años, '.$meses.' meses y '.$dias.' días';
        }
        else
        {
        return $tiempo = $anos.' años, '.$meses.' mes y '.$dias.' días';
        }
}
}
