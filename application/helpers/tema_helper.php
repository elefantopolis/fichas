<?php
    
function render_vista($vista = '', $data = '')
{
    $CI = get_instance();
    $c = $CI->router->class;
    $m = $CI->router->method;
    if ($m == "index") $m = "inicio";
    $CI->load->view('theme/header', $data);
    if ($vista == '') {
        $CI->load->view("$c/$m", $data);
    }else{
        $CI->load->view($vista, $data);
    }
    
    $CI->load->view('theme/footer', $data);
}
?>