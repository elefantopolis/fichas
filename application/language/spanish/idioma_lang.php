<?php
 //idioma inglés

$lang['idioma.titulo'] = "Dalia Córdova :: smanship and contemporary design";
$lang['idioma.servicio_cliente'] = "Servicio al Cliente";
$lang['idioma.ingles'] = "Inglés"; 
$lang['idioma.espanol'] = "Español"; 

$lang['idioma.inicio'] = "Inicio"; 
$lang['idioma.catalogo'] = "Catálogo"; 
$lang['idioma.mayoreo'] = "DaCo Mayoreo"; 
$lang['idioma.tiendas'] = "nuestras Tiendas"; 
$lang['idioma.acerca'] = "Acerca"; 

$lang['idioma.temporada'] = "Otoño-Invierno 2015";

$lang['idioma.acerca_tienda'] = "ACERCA DE LA TIENDA"; 
$lang['idioma.presentacion'] = "Presentación"; 
$lang['idioma.poliza'] = "Póliza";
$lang['idioma.talla'] = "Guía de tallas"; 
$lang['idioma.historia'] = "Historia"; 
$lang['idioma.labor'] = "Labor Social";

$lang['idioma.preguntas'] = "PREGUNTAS FRECUENTES";
$lang['idioma.preg1'] = "preg1"; 
$lang['idioma.preg2'] = "preg2"; 
$lang['idioma.preg3'] = "preg3"; 

$lang['idioma.serv_cliente'] = "SERVICIO AL CLIENTE";
$lang['idioma.cuenta'] = "MI CUENTA"; 
$lang['idioma.envio'] = "INFORMACIÓN DE ENVÍO"; 
$lang['idioma.devolucion'] = "POLÍTICAS DE DEVOLUCIÓN"; 
$lang['idioma.condiciones'] = "TÉRMINOS Y CONDICIONES";

$lang['idioma.siguenos'] = "SÍGUENOS"; 
$lang['idioma.suscribete'] = "SUSCRÍBETE";